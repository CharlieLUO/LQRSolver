#LQRSolver
This is a numerical solver for the lqr problem. it can both work on finite-horizon and infinite-horizon case.

The package depends on libarmadillo, libmgl and libitpp. So before compiling this package, make sure the depended libraries were installed on your machine.

For more information...