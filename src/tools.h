#ifndef TOOLS_H_
#define TOOLS_H_

#include <cmath>
#include <armadillo>

using namespace arma;

// compute \mat{M}^n
mat mat_pow(mat M, int order); 

/* calculate (\mat{I_n}-\mat{M})^{-1} 
   by series expansion, when |M| is small */
mat inv_expand(mat M, int order);

/* calculate (\mat{I}+\frac{\tau}{2}\mat{M})^{-1}*(\mat{I}-\frac{\tau}{2}\mat{M}) 
   by symplectic precision integration method*/
mat spim(const mat& M, const double& t, const int& N = 5);

// mat inv_pid

// return the time such as 2015-04-05 19:28:33
void get_time(char* currtime);  

int findCeilMax(std::vector<mat> vMat);
int findFloorMin(std::vector<mat> vMat);

// \mat{X} = \trans{\mat{U}} * \mat{T} * \mat{U}
void Schur(mat& U, mat X);   				

#endif
