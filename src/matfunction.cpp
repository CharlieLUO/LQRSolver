#include "matfunction.h"
#include <iostream>
using namespace arma;

MatFunction::MatFunction()
{
	n_rows = 0;
	n_cols = 0;
	matVal.set_size(0, 0);
}

MatFunction::MatFunction(int rows, int cols)
{
	matVal.set_size(rows, cols);
	n_rows = rows;
	n_cols = cols;
	pfuns.resize(rows*cols);
}

mat& MatFunction::eval(double t)
{
	for (int i = 0; i < n_rows; ++i)
	{
		for (int j = 0; j < n_cols; ++j)
		{
			matVal(i,j) = (pfuns[i*n_cols+j])(t);
		}

	}
	return matVal;
}
