#ifndef MAT_FUNCTION_H_
#define MAT_FUNCTION_H_
#include <armadillo>
#include <functional>
#include <vector>

class MatFunction
{
public:
	std::vector<std::function< double(double)> > pfuns;
	int n_rows;
	int n_cols;
	arma::mat  matVal;
	MatFunction();
	MatFunction(int rows, int cols);

	arma::mat& eval(double t);

	MatFunction operator=(MatFunction& mf)
	{
		MatFunction result(mf.n_rows, mf.n_cols);
		result.pfuns = mf.pfuns;
		// result.matVal = mf.matVal;
		return result;
	};

	void copyTo(MatFunction& mf)
	{
		mf.n_rows = n_rows;
		mf.n_cols = n_cols;
		mf.pfuns = pfuns;
		mf.matVal = matVal;
	}
};


#endif